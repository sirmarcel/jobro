# jobro

👊 *"Jo -- your bro for the job"* 👊

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/python/black) 

`jobro` is a very small library that generates SLURM jobscripts for the clusters of the [MPCDF](https://www.mpcdf.mpg.de).

It is currently not being actively developed, and is only provided as a dependency for the [`repbench`](https://marcel.science/repbench) project.
