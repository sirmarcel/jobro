from jobro import __version__
from jobro.generate import look_up_queue, min_to_h_min, generate_jobscript
from jobro.config import username, cluster


def test_version():
    assert __version__ == "0.1.0"


def test_look_up_queue():
    q = look_up_queue("draco", 2000)

    assert q == "fhi"


def test_min_to_h_min():
    h, m = min_to_h_min(123)

    assert h == 2
    assert m == 3


target_script = r"""#!/bin/bash -l

#SBATCH -o jobro.%j.out
#SBATCH -e jobro.%j.err
#SBATCH -J jobro
#SBATCH -D ./
#SBATCH --mail-type=NONE
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=40
#SBATCH --cpus-per-task=1
#SBATCH -t 166:40:00
#SBATCH --partition=fhi
export OMP_NUM_THREADS=1

python 123
lol
"""


def test_generate():
    s = generate_jobscript(duration=10000, command="python 123\nlol", cluster="draco")
    assert s.splitlines() == target_script.splitlines()

    s = generate_jobscript(duration=100, command="python 123\nlol")
    assert s.splitlines() != target_script.splitlines()


def test_configure():
    assert cluster == "talos"
    assert username == ""
