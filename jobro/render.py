"""Rendering the actual jobscript."""

from jinja2 import Template


template = r"""#!/bin/bash -l

#SBATCH -o {{ name }}.%j.out
#SBATCH -e {{ name }}.%j.err
#SBATCH -J {{ name }}
#SBATCH -D ./
#SBATCH --mail-type=NONE
#SBATCH --nodes=1
#SBATCH --ntasks-per-node={{ cores }}
#SBATCH --cpus-per-task=1
#SBATCH -t {{ h }}:{{ min }}:00
#SBATCH --partition={{ queue }}
export OMP_NUM_THREADS={{ num_threads }}

{{ command }}
"""


def render(arguments):
    """Render the jobscript."""
    return Template(template).render(arguments)
