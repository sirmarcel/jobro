from .clusters import clusters
from .render import render
from .config import cluster as default_cluster


def generate_jobscript(
    cluster=None, duration=30, command="", name="jobro", file=None, queue=None
):
    if cluster is None:
        cluster = default_cluster

    if queue is None:
        queue = look_up_queue(cluster, duration)

    h, m = min_to_h_min(duration)

    args = {
        "num_threads": 1,
        "cores": clusters[cluster]["queues"][queue]["cores"],
        "h": h,
        "min": m,
        "queue": queue,
        "name": name,
        "command": command,
    }

    if file is None:
        return render(args)
    else:
        with open(file, "w") as f:
            f.write(render(args))


def look_up_queue(cluster, duration):
    assert cluster in clusters, f"Cluster {cluster} not found!"

    for q, info in clusters[cluster]["queues"].items():
        if info["duration"] >= duration:
            return q

    raise ValueError(
        f"Couldn't find a queue for duration {duration} on cluster {cluster}."
    )


def min_to_h_min(mins):
    h, m = divmod(mins, 60.0)

    return int(h), int(m)
