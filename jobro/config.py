from environs import Env

env = Env()

with env.prefixed("JOBRO_"):
    username = env("USER", default="")
    cluster = env("CLUSTER", default="talos")
