"""The supported clusters."""

from .config import username

# queues should be in increasing order of max queuetime

clusters = {
    "draco": {
        "queues": {
            "express": {"duration": 30, "cores": 32},
            "short": {"duration": 4 * 60, "cores": 32},
            "general": {"duration": 24 * 60, "cores": 32},
            "fhi": {"duration": 7 * 24 * 60, "cores": 40},
        },
        "scratch": f"/ptmp/{username}/",
    },
    "cobra": {
        "queues": {
            "express": {"duration": 30, "cores": 40},
            "medium": {"duration": 24 * 60, "cores": 40},
        },
        "scratch": f"/ptmp/{username}/",
    },
    "eos": {
        "queues": {
            "p.24h": {"duration": 24 * 60, "cores": 32},
        },
        "scratch": f"/scratch/{username}/"
    },
    "talos": {
        "queues": {
            "p.talos": {"duration": 4 * 24 * 60, "cores": 40},
        },
        "scratch": f"/talos/scratch/{username}/"
    },
}
